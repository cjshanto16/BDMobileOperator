package teamaos.bdmobileoperator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;

public class Categories extends AppCompatActivity {

    private ImageView advertise,notifications;
    private RecyclerView services;
    Toolbar toolbar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        toolbar = findViewById(R.id.home_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        advertise = findViewById(R.id.advertisement);
        notifications = findViewById(R.id.notification);
        services = findViewById(R.id.category_recycler);

    }
}
